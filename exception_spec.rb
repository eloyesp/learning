class CustomError < RuntimeError

  attr_accessor :response

  def initialize response
    self.response = response
  end

  def message
    "error: #{response.error}"
  end

end

describe CustomError do

  let :response do
    double('Response')
  end

  it 'requires a response' do
    expect {
      raise CustomError
    }.to raise_error(ArgumentError)
  end

  it 'accepts a response that is stored' do
    expect {
      raise CustomError, response
    }.to raise_error { |error|
      expect(error.response).to eq(response)
    }
  end

  it 'the message is get from the response' do
    response.stub(:error).and_return('message')
    expect {
      raise CustomError, response
    }.to raise_error('error: message')
  end

end
